using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PocketTrigger : MonoBehaviour
{
    ScoreManager scoreManager;
    SceneLoader sceneLoader;

    private void Awake()
    {
        scoreManager = GameObject.FindGameObjectWithTag("ScoreManager").GetComponent<ScoreManager>();
        sceneLoader = FindObjectOfType<SceneLoader>();
    }
    private void OnTriggerEnter(Collider other)
    {
       
        
        if (other.tag.Equals("ball"))
        {
            scoreManager.AddScore();
            scoreManager.CheckIfWin();
            Destroy(other.gameObject);
        }
        else if (other.tag.Equals("CueBall"))
        {
            Debug.Log("Game Over");
            Destroy(other.gameObject);
            sceneLoader.Restart();
        }
       
    }
}
