using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class CueballControl : MonoBehaviour
{
    #region old logic
    //Vector3 offset;
    //float coordZ;

    //private void OnMouseDown()
    //{
    //    coordZ = Camera.main.WorldToScreenPoint(gameObject.transform.position).z;
    //    offset = gameObject.transform.position - GetMouseWorldPosition();
    //}
    //private void OnMouseDrag()
    //{
    //    transform.position = GetMouseWorldPosition() + offset;
    //    transform.position = new Vector3();
    //}

    //Vector3 GetMouseWorldPosition()
    //{
    //    Vector3 posZ = Input.mousePosition;
    //    posZ.y = coordZ;
    //    return Camera.main.ScreenToWorldPoint(posZ);
    //}
    #endregion

    #region
    //GameObject selectedObject;
    //private void Update()
    //{
    //    if (Input.GetMouseButtonDown(0))
    //    {
    //        if (selectedObject == null)
    //        {
    //            RaycastHit hit = CastRay();
    //            if (hit.collider.tag == "CueBall")
    //            {
    //                selectedObject = hit.collider.gameObject;
    //            }
    //        }
    //    }
    //    if (selectedObject!=null)
    //    {
    //        Vector3 position = new Vector3(Input.mousePosition.x, Input.mousePosition.y, Camera.main.WorldToScreenPoint(selectedObject.transform.position).z);
    //        Vector3 worldPos = Camera.main.ScreenToViewportPoint(position);
    //        selectedObject.transform.position = new Vector3(worldPos.x,selectedObject.transform.position.y,worldPos.z);
    //    }
    //}

    //private RaycastHit CastRay()
    //{
    //    Vector3 screeMousePosFar = new Vector3(Input.mousePosition.x,Input.mousePosition.y,Camera.main.farClipPlane);
    //    Vector3 screenMousePosNear = new Vector3(Input.mousePosition.x, Input.mousePosition.y, Camera.main.nearClipPlane);

    //    Vector3 worldMousePosFar = Camera.main.ScreenToWorldPoint(screeMousePosFar);
    //    Vector3 worldMousePosNear = Camera.main.ScreenToWorldPoint(screenMousePosNear);

    //    RaycastHit hit;
    //    Physics.Raycast(worldMousePosNear,worldMousePosFar - worldMousePosNear,out hit);

    //    return hit;
    //}
    #endregion
    Vector3 dist;
    Vector3 startPos;
    float posX;
    float posZ;
    float posY;
    [SerializeField] float force;
    CueDragControl cueDrag;
    Rigidbody rb;
    bool hasStart;
    private void Start()
    {
        cueDrag = FindObjectOfType<CueDragControl>();
        rb = GetComponent<Rigidbody>();
        rb.sleepThreshold = 0.1f;
    }

    private void Update()
    {
        if (cueDrag.isCueStrike )
        {
           
            cueDrag.CueStartPos();
           
           
        }
    }
    void OnMouseDown()
    {
        startPos = transform.position;
        dist = Camera.main.WorldToScreenPoint(transform.position);
        posX = Input.mousePosition.x - dist.x;
        posY = Input.mousePosition.y - dist.y;
        posZ = Input.mousePosition.z - dist.z;
    }

    private void OnMouseUp()
    {
        cueDrag.gameObject.SetActive(true);
        cueDrag.CueStartPos();
    }

    void OnMouseDrag()
    {
        float disX = Input.mousePosition.x - posX;
        float disY = Input.mousePosition.y - posY;
        float disZ = Input.mousePosition.z - posZ;
        Vector3 lastPos = Camera.main.ScreenToWorldPoint(new Vector3(disX, disY, disZ));
        transform.position = new Vector3(lastPos.x, startPos.y, lastPos.z);
        cueDrag.gameObject.SetActive(false);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag.Equals("cue"))
        {
            var cue = other.GetComponent<CueDragControl>();
            
            rb.AddForce(other.gameObject.transform.up * force * cue.forceGathered,ForceMode.Force);

            //cue.gameObject.SetActive(false);
            cue.isRelease = false;
          
            cue.CueStartPos();
            cue.isCueStrike = true;
          


        }
    }
}
