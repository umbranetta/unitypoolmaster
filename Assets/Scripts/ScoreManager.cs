using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ScoreManager : MonoBehaviour
{
    [SerializeField] int pointsNeedToWin;
    [SerializeField] int currentPoints;
    [SerializeField] TextMeshProUGUI scoreTxt,objectiveTxt;
    [SerializeField] CanvasGroup nextScenePanel;

    private void Update()
    {
        objectiveTxt.text = "Objective: score " + pointsNeedToWin.ToString()+" " + "points";
        scoreTxt.text = "Score: "+ currentPoints.ToString();
    }

    public void CheckIfWin()
    {
        if (currentPoints == pointsNeedToWin)
        {
            nextScenePanel.alpha = 1f;
            nextScenePanel.interactable = true;
            nextScenePanel.blocksRaycasts = true;
            Debug.Log("Game Won");
        }
    }

    public void AddScore()
    {
        currentPoints++;
        Debug.Log("Add score");
    }
}
