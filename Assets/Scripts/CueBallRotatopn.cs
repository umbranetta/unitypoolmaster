using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CueBallRotatopn : MonoBehaviour
{
    Vector3 strikeDirection;
    Vector3 cameraOffset,cueOffset;
    [SerializeField] GameObject cue, ball;
    Camera mainCamera;
    Quaternion cameraRot,cueRot;
    void Start()
    {
        strikeDirection = Vector3.forward;
        mainCamera = Camera.main;
        cameraOffset = ball.transform.position - mainCamera.transform.position;
        cueOffset = ball.transform.position - cue.transform.position;
        cameraRot = mainCamera.transform.rotation;
        cueRot = cue.transform.rotation;
    }

    // Update is called once per frame
    void Update()
    {
        var x = Input.GetAxis("Horizontal");
        if (x != 0)
        {
            var angle = x * 75 * Time.deltaTime;
            strikeDirection = Quaternion.AngleAxis(angle, Vector3.up) * strikeDirection;
            mainCamera.transform.RotateAround(ball.transform.position, Vector3.up , angle);
            cue.transform.RotateAround(ball.transform.position , Vector3.up, angle);
        }

        
       
      
    }

    private void LateUpdate()
    {
        //cue.transform.position = ball.transform.position - cueOffset;
        //mainCamera.transform.position = ball.transform.position - cameraOffset;
       // cue.transform.rotation = cueRot;
       // mainCamera.transform.rotation = cameraRot;
    }
}
