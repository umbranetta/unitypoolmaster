using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class CueDragControl : MonoBehaviour
{
    

   [SerializeField] private GameObject cue;
   [SerializeField] private GameObject cueBall;

    private float cueDirection = -1;
    [SerializeField] private float speed = 7;

    [SerializeField] float MIN_DISTANCE;
    [SerializeField] float MAX_DISTANCE;

    float maxClampDistance = 9f;
    float defaultDistance;
    float forceTreshold = 0.5f;
    public float forceGathered;
    float y = 0;
    Renderer cueRenderer;

    public bool isRelease ;
    public bool isCueStrike;

    void Start()
    {
        defaultDistance = Vector3.Distance(cueBall.transform.position, transform.position);
        cueRenderer = GetComponent<Renderer>();

        
    }
    public  void Update()
    {
        //transform.position = cueBall.transform.position - transform.forward * defaultDistance;
       // transform.LookAt(cueBall.transform);
        

        if (Input.GetMouseButton(0))
        {
           
            y = Input.GetAxis("Mouse Y");
            isRelease = false;
            isCueStrike = false;
            cueRenderer.enabled = true;
        }

        if (Input.GetMouseButtonUp(0))
        {
            
            if (forceGathered > defaultDistance + forceTreshold)
            {
                isRelease = true;
            }
            cueRenderer.enabled = false;

        }
        CueRlease();
        CueDrag();

       
    }

    

    void CueDrag()
    {
       // var newPosition = transform.position + transform.up * Input.GetAxis("Mouse Y");
        var newPosition = transform.position + transform.up * y;

        forceGathered = Vector3.Distance(newPosition, cueBall.transform.position);
       

        if ((forceGathered < defaultDistance + maxClampDistance) && forceGathered > defaultDistance)
        {
            transform.position = newPosition;
        }
    }

    void CueRlease()
    {
        if (isRelease)
        {
            var step = speed * Time.deltaTime;
            transform.position = Vector3.MoveTowards(transform.position, cueBall.transform.position, step);
            // StartCoroutine(MoveCueAfterStrike(transform.position, cueBall.transform.position - transform.up * defaultDistance * 1.5f, 1f));
            //CueStartPos();
            
        }
        
    }
    public void CueStartPos()
    {
        transform.position = cueBall.transform.position - transform.up * defaultDistance;
        float y = Camera.main.transform.position.y;
        Camera.main.transform.position = cueBall.transform.position - Camera.main.transform.up * defaultDistance;
        Camera.main.transform.position = new Vector3(Camera.main.transform.position.x, y,Camera.main.transform.position.z);
        forceGathered = 0;
       
    }

    public void ResetCue()
    {
        StartCoroutine(ResetCueCoroutine());
    }
    IEnumerator ResetCueCoroutine()
    {
        yield return new WaitForSeconds(3f);
        cueRenderer.enabled = true;
    }
   public IEnumerator MoveCueAfterStrike(Vector3 source, Vector3 target, float overTime)
    {
        float startTime = Time.time;
        while (Time.time < startTime + overTime)
        {
            transform.position = Vector3.Lerp(source, target, (Time.time - startTime) / overTime);
            Debug.Log("cue move");
            yield return null;
        }
        transform.position = target;
    }

   

   
   

   
   

   
}
